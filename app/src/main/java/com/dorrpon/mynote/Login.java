package com.dorrpon.mynote;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    private EditText email, password;
    private Button btn_login;
    private ProgressBar loading;
    private SharedPreferences store;
    private String URL;
    String user_id, user_name, user_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        String HOST = getString(R.string.host);

        URL = HOST + "/api/auth/login";
        Log.d("TAGG",URL);


        store = getApplicationContext().getSharedPreferences("Store", 0);


        loading = findViewById(R.id.loading);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        btn_login = findViewById(R.id.btn_login);


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mEmail = email.getText().toString().trim();
                String mPass = password.getText().toString().trim();


                if (!mEmail.isEmpty() || !mPass.isEmpty()) {
                    submit();
                } else {
                    email.setError("Please Insert Email");
                    password.setError("Please Insert Password");
                }
            }
        });


    }


    public void submit() {

        loading.setVisibility(View.VISIBLE);
        btn_login.setVisibility(View.GONE);


        String mEmail = email.getText().toString().trim();
        String mPass = password.getText().toString().trim();
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("email", mEmail);
            jsonBody.put("password", mPass);
            final String mRequestBody = jsonBody.toString();

            Log.d("TAGG", jsonBody.toString());

            JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, URL, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject data) {
                            loading.setVisibility(View.GONE);
                            btn_login.setVisibility(View.GONE);
                            Log.d("TAGG", data.toString());
//                           String token = null;


                            try {
//                                token = data.getString("access_token");
                                user_id = data.getString("id");
                                user_name = data.getString("name");
                                user_email = data.getString("email");


                                SharedPreferences.Editor editor = store.edit();
//                                editor.putString("token", token);
                                editor.putString("user_id", user_id);
                                editor.putString("user_name", user_name);
                                editor.putString("user_email", user_email);
                                editor.apply();

                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                                finish();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                loading.setVisibility(View.GONE);
                                btn_login.setVisibility(View.VISIBLE);
                                Toast.makeText(getApplicationContext(), "Email/Password is Wrong", Toast.LENGTH_SHORT).show();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.setVisibility(View.GONE);
                    btn_login.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(), "Something Wrong. Try Again", Toast.LENGTH_SHORT).show();
                }
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() {
                    return mRequestBody.getBytes(StandardCharsets.UTF_8);
                }
            };


            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}