package com.dorrpon.mynote;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    SwipeRefreshLayout refresh;
    SharedPreferences store;
    String HOST;
    String user_id, user_name, user_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        store = MainActivity.this.getSharedPreferences("Store", 0);

        user_id = store.getString("user_id", "");
        user_name = store.getString("user_name", "");
        user_email = store.getString("user_email", "");

        refresh = findViewById(R.id.refresh);
        HOST = getString(R.string.host);
        noteLoad();


        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                noteLoad();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menuAdd) {
//            Toast.makeText(this, "Add New note", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(MainActivity.this, OCRActivity.class);
            startActivity(intent);
            // do something here
        } else if (id == R.id.menuLogout) {
            Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show();
            logout();
        }
        return super.onOptionsItemSelected(item);
    }


    public void noteLoad() {
        final ProgressDialog dialog = ProgressDialog.show(MainActivity.this, "",
                "Loading. Please wait...", true);


        final String URL = HOST + "/api/notes?id=" + user_id;
        Log.d("TAGG", URL);

        final RecyclerView notes = findViewById(R.id.noteList);
        notes.setLayoutManager(new LinearLayoutManager(this));
        //here is the view
        StringRequest request = new StringRequest(URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                refresh.setRefreshing(false);
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                Note[] routes = gson.fromJson(response, Note[].class);


                if (response.equals("[]")) {

                    AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                    dialog.setCancelable(true);
                    dialog.setTitle("Info");
                    dialog.setMessage("No Note Found");
                    final AlertDialog alert = dialog.create();
                    alert.show();
                    notes.setAdapter(new NoteAdapter(MainActivity.this, routes));

                } else {
                    notes.setAdapter(new NoteAdapter(MainActivity.this, routes));

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();
                Toast.makeText(MainActivity.this, "Something Wrong", Toast.LENGTH_SHORT).show();
//
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    public void logout() {

        Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show();
        SharedPreferences.Editor editor = store.edit();
        editor.clear();
        editor.apply();
        Intent in = new Intent(MainActivity.this, Login.class);
        startActivity(in);
    }
}
