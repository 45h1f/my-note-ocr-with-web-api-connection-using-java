package com.dorrpon.mynote;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class NewActivity extends AppCompatActivity {

    ImageView imageView;
    Button submit;
    EditText title, note;
    String s_title, s_note;
    String HOST, URL;
    ProgressBar loading;
    String user_id;
    SharedPreferences store;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);
        imageView = findViewById(R.id.imageView);
        submit = findViewById(R.id.submit);
        title = findViewById(R.id.title);
        note = findViewById(R.id.note);
        loading = findViewById(R.id.loading);
        HOST = getString(R.string.host);
        URL = HOST + "/api/notes";
        store = getApplicationContext().getSharedPreferences("Store", 0);
        user_id = store.getString("user_id", "");

        Intent intent = getIntent();
        Bundle IntentData = intent.getExtras();

        String result = (String) IntentData.get("result");

        note.setText(result);

//        if (getIntent().hasExtra("image")) {
//            Bitmap b = BitmapFactory.decodeByteArray(
//                    getIntent().getByteArrayExtra("image"), 0, getIntent().getByteArrayExtra("image").length);
//            imageView.setImageBitmap(b);
//            imageView.setVisibility(View.VISIBLE);
//        } else {
//            imageView.setVisibility(View.GONE);
//        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (TextUtils.isEmpty(title.getText()) || TextUtils.isEmpty(note.getText())) {
                    if (TextUtils.isEmpty(title.getText())) {
                        title.setError("Title is required");

                    }
                    if (TextUtils.isEmpty(note.getText())) {
                        note.setError("Note is required!");

                    }

                } else {
                    Toast.makeText(NewActivity.this, "Submitting....", Toast.LENGTH_SHORT).show();
                    submit();
                    fileSave();
                }

            }
        });
    }

    public void submit() {
        s_title = title.getText().toString();
        s_note = note.getText().toString();


        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("user_id", user_id);
            jsonBody.put("title", s_title);
            jsonBody.put("note", s_note);

            final String mRequestBody = jsonBody.toString();

            Log.d("TAGG", mRequestBody);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Toast.makeText(getApplicationContext(), "Cloud Save Successfully", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Cloud Save Failed", Toast.LENGTH_SHORT).show();
                    loading.setVisibility(View.GONE);
                    submit.setVisibility(View.VISIBLE);
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() {
                    return mRequestBody == null ? null : mRequestBody.getBytes(StandardCharsets.UTF_8);
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }

                @Override
                public Map<String, String> getHeaders() {
                    String getToken = getIntent().getStringExtra("user_id");
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    headers.put("user_id", user_id);
                    return headers;
                }

            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void fileSave() {
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();
        if (isExternalStorageWritable()) {
//            saveImage(bitmap);
            textSave();
        } else {
            Toast.makeText(this, "Failed to save Image", Toast.LENGTH_SHORT).show();
        }


    }

    private void saveImage(Bitmap finalBitmap) {

        s_title = title.getText().toString();
        s_note = note.getText().toString();

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/myNote/" + s_title);
        myDir.mkdirs();

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fname = s_title + timeStamp + ".jpg";

        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            Toast.makeText(this, "Save Image", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void textSave() {
        s_title = title.getText().toString();
        s_note = note.getText().toString();

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/myNote/" );
        myDir.mkdirs();

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fname = s_title + timeStamp + ".txt";

        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);


            out.write(s_note.getBytes());


            out.flush();
            out.close();
            Toast.makeText(this, "Text Saved", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }
}
