package com.dorrpon.mynote;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SingleActivity extends AppCompatActivity {
    String HOST, URL, s_title, s_note;
    SharedPreferences store;
    TextView title, note;
    Button delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single);
        store = getApplicationContext().getSharedPreferences("Store", 0);
//        TOKEN = store.getString("token", "");
        HOST = getString(R.string.host);

        Intent intent = getIntent();
        Bundle IntentData = intent.getExtras();

        final String id = (String) IntentData.get("id");

        URL = HOST + "/api/notes/" + id;

        final ProgressDialog dialog = ProgressDialog.show(SingleActivity.this, "",
                "Loading. Please wait...", true);
        dialog.setCanceledOnTouchOutside(true);

        delete = findViewById(R.id.delete);

        title = findViewById(R.id.title);
        note = findViewById(R.id.note);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(SingleActivity.this)
                        .setTitle("Delete")
                        .setMessage("Are you sure? you want to delete?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                delete();
                            }
                        })
                        .setNegativeButton("No", null).show();


            }
        });

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    dialog.dismiss();
                    s_title = response.getString("title");
                    s_note = response.getString("note");


                    note.setText(s_note);
                    title.setText(s_title);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }


        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
//                headers.put("title", TOKEN);
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);


    }

    public void delete() {
        final ProgressDialog dialog2 = ProgressDialog.show(SingleActivity.this, "",
                "Loading. Please wait...", true);
        dialog2.setCanceledOnTouchOutside(true);

        delete = findViewById(R.id.delete);

        title = findViewById(R.id.title);
        note = findViewById(R.id.note);


        JsonObjectRequest request2 = new JsonObjectRequest(Request.Method.DELETE, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog2.dismiss();
                Toast.makeText(SingleActivity.this, "Delete Successfully", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(SingleActivity.this, MainActivity.class);
                startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog2.dismiss();
                dialog2.dismiss();
                Toast.makeText(SingleActivity.this, "Delete Successfully", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(SingleActivity.this, MainActivity.class);
                startActivity(intent);
            }


        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
//                headers.put("title", TOKEN);
                return headers;
            }
        };
        RequestQueue queue2= Volley.newRequestQueue(this);
        queue2.add(request2);


    }

}
